import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qoranios/Activity/list_act.dart';
import 'package:qoranios/Activity/surelist_act.dart';
import '../Classes/database_helper .dart';
class Page1Btn extends StatelessWidget
{
  String str="";
  Page1Btn(String str)
  {
    this.str=str;
  }
  var borderData=BorderSide(
    color: Color(0xFF92582b), //Color of the border
    style: BorderStyle.solid, //Style of the border
    width: 1, //width of the border
  );
  @override
  Widget build(BuildContext context) {
    return  DecoratedBox(
        decoration:
        ShapeDecoration(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)), color: Color(0xFFfdfaf0)),
        child: Theme(
          data: Theme.of(context).copyWith(
              buttonTheme: ButtonTheme.of(context).copyWith(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap)),
          child: OutlineButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Text(str),
            onPressed: ()=>btn_click(context),
            borderSide: borderData,
          ),
        ),
      );
  }
  void btn_click(BuildContext context)
  {
    var item = ["قرآن کریم", "تلاوت مجلسی", "آموزش", "متفرقه", "ارتباط با ما"];
    if(str.contains("Eng"))
      {
        print("Eng");
        _query();
      }
    else if(str.contains("Far"))
    {
      /*Navigator.push(
        context,
        //CupertinoPageRoute(builder: (context) => list_act()),
        MaterialPageRoute(builder: (context) => list_act(item)),
      );*/
    }
  }
  void _query() async
  {
    /*final dbHelper = DatabaseHelper.instance;
    final allRows = await dbHelper.queryAllRows();
    print('query all rows:');
    allRows.forEach(print);*/

    DictionaryDataBaseHelper db=new DictionaryDataBaseHelper();
    db.init();
  }
}

class MyTextBtn extends StatelessWidget
{
  String txt;
  int ind=0;//0=normal   1=bold
  int type=1;
  MyTextBtn(String txt,int ind,int type)
  {
    this.txt=txt;
    this.ind=ind;
    this.type=type;//1=sure   2=joz   3=manabe
  }
  @override
  Widget build(BuildContext context)
  {
    TextButton tb;
    if(ind==0)
      {
        tb=TextButton(onPressed: btn_click, child: Text(txt,style: TextStyle(color: Color(0xFFa0998f),fontWeight: FontWeight.normal),));
      }
    else if(ind==1)
      {
        tb=TextButton(onPressed: btn_click, child: Text(txt,style: TextStyle(color: Color(0xFFa0998f),fontWeight: FontWeight.bold),));
      }
    return tb;
  }
  void btn_click()
  {
    print(txt);

  }
}

