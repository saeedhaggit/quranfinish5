import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qoranios/Activity/surelist_act.dart';
//import '../Widget/MyButton.dart';
import '../Classes/database_helper .dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class matn_act extends StatefulWidget
{
  int type=0;//1=sure   2=juz    3=manabe
  int index=0;
  DictionaryDataBaseHelper db;
  List data=new List();
  matn_act(int type,int index,DictionaryDataBaseHelper db)
  {
    this.type=type;
    this.index=index;
    this.db=db;
    print("type=$type    index=$index");

    //setData();
  }

  @override
  _matn_actState createState() => _matn_actState();
}

class _matn_actState extends State<matn_act>
{
  bool visible_top=false;
  bool visible_bot=true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
    setData();
  }
  setData() async
  {
    if(widget.type==1)
    {
      widget.data=await widget.db.getSure(widget.index) as List   ;
      //print("bbbbb=$data");
      setState(() {

      });
    }
  }
  @override
  Widget build(BuildContext context)
  {
    print("data.length=${widget.data.length}");
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            color: Color(0xFFfef8ea),
          ),
          Container(
            width: double.infinity,
            height: 32.0,
            color: Color(0xFFfaf0d1),
            child: Align(
              child: Text(
                "جز | احزاب",
                style: TextStyle(fontSize: 10.0, color: Colors.black),
              ),
              alignment: AlignmentDirectional.center,
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(5.0, 35.0, 5.0, 35.0),
            child: ListView.builder(
              itemCount: widget.data.length,
              itemBuilder: (context, position) {
                return Material(
                  child: ListTile(
                    title: Text("${widget.data[position]['text']}"),
                  ),
                );
              },
            ),
          ),
          Visibility(
            visible: visible_top,
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: double.infinity,
                height: 64.0,
                color: Color(0xFF645f5b),
              ),
            ),
          ),
          Visibility(
            visible: visible_bot,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                height: 64.0,
                color: Color(0xFF645f5b),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
